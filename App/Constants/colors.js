export const white = '#FFFFFF';
export const grey = '#A9A9A9';
export const bottomBorder = '#26A69A';
export const red = '#FF0000';
export const green = '#00FF00';
export const lightGrey = '#a6a6a6';
export const darkBlack = '#000000';
