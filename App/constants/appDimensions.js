export const zero = 0;
export const one = 1;
export const two = 2;
export const ten = 10;
export const twelve = 12;
export const fifteen = 15;
export const twenty = 20;
export const twentyFour = 24;
