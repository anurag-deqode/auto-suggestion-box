import React, {useState, useEffect, useCallback} from 'react';
import {StyleSheet, View} from 'react-native';
import Autocomplete from '../../components/autocomplete';
import * as Colors from '../../constants/colors';
import {getSuggestions} from '../../services/api';
import {debounce} from '../../services/debounce';

const index = () => {
  const [data, setData] = useState([]);
  const [query, setQuery] = useState('');
  const [fetchedText, setFetchedText] = useState('');

  useEffect(() => {
    debounceWrapper(query);
  }, [query]);

  const debounceWrapper = useCallback(
    debounce((query) => fetchData(query), 500),
    [],
  );

  const fetchData = (text) => {
    setQuery(text);
    const textArray = text.split(' ');
    const newText = textArray[textArray.length - 1];
    setFetchedText(newText);

    if (!text || newText === '') {
      setData([]);
      return null;
    }
    getSuggestions(newText)
      .then((res) => {
        setData(res);
      })
      .catch(() => {
        setData([]);
      });
  };
  return (
    <View style={styles.container}>
      <Autocomplete
        query={query}
        data={data}
        fetchData={fetchData}
        fetchedText={fetchedText}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.grey,
    flex: 1,
  },
});

export default index;
