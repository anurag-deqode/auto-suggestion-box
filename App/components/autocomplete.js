import React from 'react';
import {Text, FlatList, StyleSheet, View} from 'react-native';
import * as Colors from '../constants/colors';
import * as appDimensions from '../constants/appDimensions';
import SearchBar from './searchBar';
import PropTypes from 'prop-types';

const renderItem = (item, fetchText, fetchData, query) => {
  const modifidiedTextArray = takeOutHighlightedText(item, fetchText);
  return (
    <Text
      style={styles.flatList}
      onPress={() => onPress(item, query, fetchData)}>
      {modifidiedTextArray[0]}
      <Text style={styles.highlightText}>{fetchText}</Text>
      {modifidiedTextArray[1]}
    </Text>
  );
};

const takeOutHighlightedText = (item, query) => {
  if (query != '') {
    const stringArray = item.split(query);
    return stringArray;
  }
  return [item];
};

const onPress = (item, query, fetchData) => {
  const newArray = query.split(' ');
  newArray.splice(newArray.length - 1, 1);
  let newString = '';
  newArray.map((item) => {
    newString = newString + item + ' ';
  });
  fetchData(`${newString + item} `);
};

const Autocomplete = (props) => {
  return (
    <>
      <SearchBar query={props.query} fetchData={props.fetchData} />
      <View style={styles.suggestionBox}>
        <FlatList
          data={props.data}
          keyExtractor={(index) => index.toString()}
          renderItem={({item}) =>
            renderItem(item, props.fetchedText, props.fetchData, props.query)
          }
        />
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  flatList: {
    paddingLeft: appDimensions.fifteen,
    paddingVertical: appDimensions.fifteen,
    fontSize: appDimensions.twenty,
    backgroundColor: Colors.white,
    borderTopWidth: appDimensions.one,
    color: Colors.lightGrey,
    borderColor: Colors.lightGrey,
  },
  highlightText: {
    color: Colors.darkBlack,
  },
  suggestionBox: {
    marginHorizontal: appDimensions.twelve,
    borderRadius: appDimensions.twelve,
  },
});

Autocomplete.prototype = {
  query: PropTypes.string,
  fetchData: PropTypes.func,
  fetchedText: PropTypes.string,
  data: PropTypes.array,
};

export default Autocomplete;
