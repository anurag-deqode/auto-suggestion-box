import React from 'react';
import {TextInput, StyleSheet, View} from 'react-native';
import * as Colors from '../constants/colors';
import * as appDimensions from '../constants/appDimensions';
import PropTypes from 'prop-types';

const SearchBar = (props) => {
  return (
    <View style={styles.searchBar}>
      <TextInput
        onChangeText={props.fetchData}
        value={props.query}
        placeholder="Type Here..."
        style={styles.textInput}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  searchBar: {
    backgroundColor: Colors.white,
    flexDirection: 'row',
    borderTopLeftRadius: appDimensions.twelve,
    borderTopRightRadius: appDimensions.twelve,
    margin: appDimensions.twelve,
    marginBottom: appDimensions.zero,
  },
  textInput: {
    fontSize: appDimensions.twentyFour,
    backgroundColor: Colors.white,
    width: '85%',
    marginLeft: appDimensions.ten,
  },
});

SearchBar.prototype = {
  query: PropTypes.string,
  fetchData: PropTypes.func,
};

export default SearchBar;
