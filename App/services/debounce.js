export const debounce = (fun, delay) => {
  let debounceTimer = null;
  function debouncer(...args) {
    clearTimeout(debounceTimer);
    debounceTimer = setTimeout(() => fun(...args), delay);
  }
  return debouncer;
};
