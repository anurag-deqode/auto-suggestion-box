# auto-suggestion-box

Demonstration of `auto-suggestion-box` for giving suggestions for the text being typed in a input box.

### Screencast

![screencast](/screencast/screencast.gif)

### System requirements

Make sure you have a complete environment [setup](https://reactnative.dev/docs/environment-setup) for react-native to run this demo.

### Installing Dependencies

clone the repository:

```sh
$ git clone https://gitlab.com/anurag-deqode/auto-suggestion-box.git
```

Go to project's root directory:

```sh
$ cd auto-suggestion-box
```

Install all the dependencies using your preferred package manager:

```sh
$ yarn install
or
$ npm install
```

For ios:

```sh
$ npx pod-install
or
$ cd ios/ && pod install && cd ..
```

### Run Apps

Run iOS

```sh
$ yarn ios
```

Run Android

- Run the emulator
- Run the following command

```sh
$ yarn android
```

`Note`: Sometime error can cause while creating android build, so try to create the build through android studio and run on the android emulator.

### Props

| Prop      | Type  | Required | Description                                                                 |
| --------- | ----- | -------- | --------------------------------------------------------------------------- |
| data      | Array | true     | It takes the array of strings for the suggestions                           |
| fetchData | fun   | true     | It gives the suggestion from api when user enter the text in the search bar |

### Code Description

- `Home screen`: On the home screen, AutoComplete component is called and defination of fetchData function is provided FetchData function is used to fetch suggestion from API.
- `SearchBar`: SearchBar component contains textInput and it send the input to the getSuggestion function.
- `Autocomplete`: Autocomplete complete contains Searchbar component and flatList. When user enter the text in SearchBar then the content will be shown in FlatList.
- `API`: API file is present in Services which gives the suggestion..
- `Styling`: Used Stylesheet to avoid inline styling. For goof readablity of code.
- `Debounce`: Use Debounce feature so that limited number of API will hit.
